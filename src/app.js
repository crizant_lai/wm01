import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import NavBar from './components/navBar/NavBar.jsx'
import ReactLazyLoadContainer from './components/listView/ReactLazyLoadContainer.jsx'
import RegistrationForm from './components/registrationForm/RegistrationForm.jsx'

render(
  <Provider store={store}>
    <NavBar />
  </Provider>,
  document.getElementById('nav_wrapper')
)

if (document.getElementById('react_lazyload_container')){
  render(
    <Provider store={store}>
      <ReactLazyLoadContainer />
    </Provider>,
    document.getElementById('react_lazyload_container')
  )
}

if (document.getElementById('registrationFormWrapper')){
  render(
    <Provider store={store}>
      <RegistrationForm />
    </Provider>,
    document.getElementById('registrationFormWrapper')
  )
}
