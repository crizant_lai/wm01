import React from 'react'

class ListViewItem extends React.Component{
  render(){
    const { article } = this.props
    return (
      <div class="blog_listing__item">
				<a href={article.url}>
					<div class="blog_listing__item__img ">
						<img src={article.imgSrc} />
						{
              (article.editor_pick==true) &&
              <div class="editor_pick">精選</div>
            }
					</div>
					<div class="blog_listing__item__content">
						<div class="blog_listing__item__content__tag">
							<tag>
                { article.tag }
							</tag>
						</div>
						<div class="blog_listing__item__content__tit">
							<h3>
                { article.title }
							</h3>
						</div>
						<div class="blog_listing__item__content__caption">
              { article.caption }
						</div>
						<div class="blog_listing__item__content__time">
              <span class="clock">
                { article.created }
              </span>
            </div>
					</div>
				</a>
			</div>
    )
  }
}

export default ListViewItem
