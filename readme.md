# Readme

## UX Suggestions
1. Problem: The titles on the "熱門新聞排行榜" at right side of the page is cut into short lines, which is hard to read.  
  Suggestion: Use squared images, then the title can be longer for each line.
2. Problem: When scrolling down on the infinity list view, the space at the right side is empty, which is a waste.  
   Suggestion: Make "熱門新聞排行榜" sticky on the right, so that it sticks at the right side, no matter how much we scrolled.

## How to run
1. Clone the repository
2. Navigate to the repository folder  
   ``` cd wm01 ```
3. Install live-server using npm:  
   ``` npm i -g live-server ```
4. Navigate to the public folder  
   ``` cd public ```
5. Launch live-server, a browser window will be automatically opened.  
   ``` live-server ```
6. To See the registration form, visit the following url:  
   ``` http://127.0.0.1:8080/registration.html ```

## How to build
1. Install webpack globally  
   ``` npm i -g webpack ```
2. Install the project dependencies  
   ``` npm install ```
3. Build with
   ``` webpack ```
   or
   ``` webpack -w ``` for development

## Styles
1. There is a .less file in the public/css/ folder, use your favourite tool to compile it into .css file.

## Others
1. The redirection for non-https has been removed, it should be handled at server side.
