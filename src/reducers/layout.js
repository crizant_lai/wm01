const defaultState = {
  showSubMenu: false
}

const layout = (state={...defaultState}, action) =>{
  switch (action.type) {
    case 'TOGGLE_SUBMENU':
      return {
        ...state,
        showSubMenu: !state.showSubMenu
      }

    case 'COLLAPSE_SUBMENU':
      return {
        ...state,
        showSubMenu: false
      }

    default:
      return state
  }
}

export default layout
