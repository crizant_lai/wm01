import React from 'react'
import classNames from 'classnames'

class Submenu extends React.Component{
  render(){
    const submenuClassnames = {
      'menu__submenu': true,
      'fix': this.props.fixed
    }
    return (
      <nav id="touch_submenu" class={classNames(submenuClassnames)}>
			  <div id="touch_submenu_wrapper_container" class="wrapper">
  				<div class="touch_submenu touch_submenu_wrapper">
  					<div class="menu__submenu__search" data-search-form="">
							<input type="search" name="keyword" placeholder="搜尋" />
							<div class="btn">搜尋</div>
  					</div>
  				</div>
					<ul class="menu__submenu--list">
						<li><a href="http://www.wm01.com/">主頁</a></li>
						<li><a href="http://www.wm01.com/module/hotnews">熱門文章</a></li>
						<li><a href="http://www.wm01.com/module/latestnews">最新文章</a></li>
						<li><a href="https://www.wm01.com/channel/%E6%96%B0%E8%81%9E">新聞</a></li>
						<li><a href="http://2017hkceelection.wm01.com/">特首選舉</a></li>
						<li><a href="https://www.wm01.com/section/%E6%B8%AF%E8%81%9E">港聞</a></li>
						<li><a href="https://www.wm01.com/section/%E5%85%A9%E5%B2%B8">兩岸</a></li>
						<li><a href="https://www.wm01.com/section/%E7%B6%93%E6%BF%9F">經濟</a></li>
						<li><a href="https://www.wm01.com/section/%E7%92%B0%E4%BF%9D">環保</a></li>
						<li><a href="https://www.wm01.com/section/%E7%BD%AA%E6%A1%88">罪案</a></li>
						<li><a href="https://www.wm01.com/channel/%E7%A4%BE%E5%8D%80">社區</a></li>
						<li><a href="https://www.wm01.com/channel/%E5%A8%9B%E6%A8%82">娛樂</a></li>
						<li><a href="https://www.wm01.com/channel/%E9%9B%BB%E5%BD%B1">電影</a></li>
						<li><a href="https://www.wm01.com/channel/%E9%9F%B3%E6%A8%82">音樂</a></li>
						<li><a href="https://www.wm01.com/channel/%E9%9F%93%E8%BF%B7">韓迷</a></li>
						<li><a href="https://www.wm01.com/section/%E5%9C%8B%E9%9A%9B">國際</a></li>
						<li><a href="https://www.wm01.com/channel/%E5%A5%B3%E7%94%9F">女生</a></li>
						<li><a href="https://www.wm01.com/section/%E5%A5%BD%E7%94%9F%E6%B4%BB">好生活</a></li>
						<li><a href="https://www.wm01.com/channel/%E8%A6%AA%E5%AD%90">親子</a></li>
						<li><a href="https://www.wm01.com/channel/%E5%AF%B5%E7%89%A9">寵物</a></li>
						<li><a href="https://www.wm01.com/channel/01%E6%95%99%E7%85%AE">01教煮</a></li>
						<li><a href="https://www.wm01.com/section/%E9%AB%94%E8%82%B2">體育</a></li>
						<li><a href="https://www.wm01.com/section/%E8%B7%91%E6%AD%A5">跑步</a></li>
						<li><a href="https://www.wm01.com/channel/%E7%86%B1%E8%A9%B1">熱話</a></li>
						<li><a href="https://www.wm01.com/channel/%E7%A7%91%E6%8A%80%E7%8E%A9%E7%89%A9">科技玩物</a></li>
						<li><a href="http://philosophy.wm01.com/">01哲學</a></li>
						<li><a href="https://www.wm01.com/section/01%E8%A7%80%E9%BB%9E">01觀點</a></li>
						<li><a href="https://www.wm01.com/blogger/01%E5%8D%9A%E8%A9%95">01博評</a></li>
						<li><a href="http://www.wm01.com/bloggers">博評作者</a></li>
						<li><a href="https://www.wm01.com/channel/01%E5%81%B5%E6%9F%A5">01偵查</a></li>
						<li><a href="https://www.wm01.com/photostory">01影像</a></li>
						<li><a href="http://www.wm01.com/tag/9972">01 Video</a></li>
						<li><a href="https://www.wm01.com/section/01%E6%B4%BB%E5%8B%95">01活動</a></li>
						<li><a href="http://space.wm01.com/">01空間</a></li>
						<li><a href="https://www.wm01.com/channel/%E6%9C%83%E5%93%A1%E5%B0%88%E5%8D%80">會員專區</a></li>
						<li><a href="https://www.wm01.com/issue">01議題</a></li>
          </ul>
          <div class="static_links">
            <ul>
              <li><a href="https://www.wm01.com/Pages/Hotline">01線報</a></li>
              <li><a href="https://www.wm01.com/Pages/PrivacyTerms">私隱條例</a></li>
              <li><a href="https://www.wm01.com/mediakit/index.html">廣告查詢</a></li>
              <li><a href="https://www.wm01.com/Pages/ContactUs">聯絡我們</a></li>
              <li><a href="https://www.wm01.com/Pages/Recruitment">加入我們</a></li>
            </ul>
            <div class="socialmedia_channel">
              <a href="https://www.facebook.com/wm01wemedia" target="_blank">
                <img src="./wm01_files/facebook_channel.png" width="50" />
              </a>
              <a href="http://www.youtube.com/c/%E9%A6%99%E6%B8%AF01WeMedia" target="_blank">
                <img src="./wm01_files/youtube_channel.png" width="50" />
              </a>
              <a href="https://www.instagram.com/wm01wemedia" target="_blank">
                <img src="./wm01_files/instagram_channel.png" width="50" />
              </a>
            </div>
          </div>
				</div>
		  </nav>
    )
  }
}

export default Submenu
