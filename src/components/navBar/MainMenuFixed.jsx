import React from 'react'
import classNames from 'classnames'
import menuItems from './menuItems'
import ToggleBtn from './ToggleBtn.jsx'

class MainMenuFixed extends React.Component{
  render(){
    const navClassNames = {
      'menu__main--fix': true,
      'sticky': (this.props.sticky===true)
    }
    return (
      <nav class={classNames(navClassNames)}>
        <div class="menu__main--inner">
          <div class="wrapper">
            <div class="menu__main--fix__logo">
              <a href="https://www.wm01.com/">
                <img src="./wm01_files/logo.png" alt="香港01" />
              </a>
            </div>
            <div class="menu__main--fix--current">
              <a href="https://www.wm01.com/channel/01%E8%A7%80%E9%BB%9E">01觀點</a>
            </div>
            <div class="menu__main--fix__scroll">
              <ul class="menu__main--fix_container float_left">
                {
                  menuItems.map(item => {
                    return (
                      <li class="menu__main--fix--tit">
                        <a href={item.url}>
                          { item.name }
                        </a>
                      </li>
                    )
                  })
                }
                </ul>
              </div>

              <div class="btn_member float_right"></div>
              <div class="btn_search float_right"></div>

              <ToggleBtn />
            </div>
          </div>
        </nav>
    )
  }
}

export default MainMenuFixed
