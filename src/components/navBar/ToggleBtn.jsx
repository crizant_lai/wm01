import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import * as layoutActions from '../../actions/layout'

class ToggleBtn extends React.Component {
  constructor(props){
    super(props)
    this.toggleSubmenu = this.toggleSubmenu.bind(this)
  }
  toggleSubmenu(){
    this.props.toggleSubmenu()
  }
  render() {
    const { showSubMenu } = this.props.layout
    const toggleBtnClassNames = {
      'menu-toggle-btn': true,
      'open': showSubMenu
    }
    return (
      <div class="btn_more float_right" onClick={this.toggleSubmenu}>
        <div class={classNames(toggleBtnClassNames)}>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    layout: state.layout
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleSubmenu: () => {
      dispatch(
        layoutActions.toggleSubmenu()
      )
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ToggleBtn)
