import React from 'react'
import menuItems from './menuItems'
import ToggleBtn from './ToggleBtn.jsx'

class MainMenu extends React.Component{
  render(){
    return (
      <nav class="menu__main" ref={(m)=>{this.dom=m}}>
        <div class="wrapper">
          <div class="menu__main__scroll">
            <ul class="menu__main_container float_left">
              {
                menuItems.map(item => {
                  return (
                    <li class="menu__main--tit">
                      <a href={item.url}>
                        { item.name }
                      </a>
                    </li>
                  )
                })
              }
            </ul>
          </div>

          <ToggleBtn />
        </div>
      </nav>
    )
  }
}

export default MainMenu
