import React from 'react'
import { connect } from 'react-redux'
import MainMenu from './MainMenu.jsx'
import MainMenuFixed from './MainMenuFixed.jsx'
import Submenu from './Submenu.jsx'

class NavBar extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      showFixedMenu: false
    }
    this.scrollListener = this.scrollListener.bind(this)
  }
  componentDidMount(){
    window.addEventListener('scroll', this.scrollListener)
  }
  componentWillUnmount(){
    window.removeEventListener('scroll', this.scrollListener)
  }
  scrollListener(){
    const el = this.mainMenu.dom
    const elBottom = el.offsetTop + el.offsetHeight
    const scrollTop = document.documentElement.scrollTop
      || document.body.scrollTop
      || 0
    //scrolled through main menu
    this.setState({
      showFixedMenu: (elBottom < scrollTop)
    })
  }
  render(){
    const { showFixedMenu } = this.state
    const { showSubMenu } = this.props.layout
    return (
      <div>
        <MainMenu ref={(m)=>{this.mainMenu=m}}/>
        <MainMenuFixed sticky={showFixedMenu}/>
        {
          (showSubMenu===true)&&
          <Submenu fixed={showFixedMenu}/>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    layout: state.layout
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavBar)
