const toggleSubmenu = () => {
  return {
    type: 'TOGGLE_SUBMENU'
  }
}

const collapseSubmenu = () => {
  return {
    type: 'COLLAPSE_SUBMENU'
  }
}

export {
  toggleSubmenu,
  collapseSubmenu
}
