import React from 'react'
import { connect } from 'react-redux'
import throttle from 'throttle-debounce/throttle'
import ListViewItem from './ListViewItem.jsx'
import * as listViewActions from '../../actions/listView'

class ReactLazyLoadContainer extends React.Component{
  constructor(props){
    super(props)
    //not to be called again within 1 second
    this.scrollListener = throttle(
      1000,
      this.scrollListener.bind(this)
    )
  }
  componentDidMount(){
    window.addEventListener('scroll', this.scrollListener)
  }
  componentWillUnmount(){
    window.removeEventListener('scroll', this.scrollListener)
  }
  scrollListener(){
    const el = this.containerInner
    const elBottom = el.offsetTop + el.offsetHeight
    const scrollTop = document.documentElement.scrollTop
      || document.body.scrollTop
      || 0
    const windowBottom = scrollTop + window.innerHeight
    if ( elBottom < windowBottom){
      //bottom is reached
      this.props.getArticles()
    }
  }
  render(){
    const { articles } = this.props
    return (
      <div class="react_lazyload_container_inner"
        ref={(el)=>{this.containerInner=el}}>
        {
          articles.map(article=>{
            return (
              <ListViewItem article={article} />
            )
          })
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    articles: state.lazyLoadArticles
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getArticles: () => {
      dispatch(
        listViewActions.getArticles()
      )
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReactLazyLoadContainer)
