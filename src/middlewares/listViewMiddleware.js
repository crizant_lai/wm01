import * as listViewActions from '../actions/listView'
const listViewMiddleware = store => next => action => {

  if (action.type=='GET_ARTICLES'){
    fetch(
      './json/getArticles.json',
      {
        method: 'GET'
      }
    ).then(
      response => response.json()
    ).then(response => {
      store.dispatch(
        listViewActions.getArticlesSuccess(response.articles)
      )
    })
  }

  next(action)
}

export default listViewMiddleware
