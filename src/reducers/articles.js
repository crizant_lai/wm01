const articles = (state=[], action) =>{
  switch (action.type) {
    case 'GET_ARTICLES_SUCCESS':
      return [
        ...state,
        ...action.articles
      ]

    default:
      return state
  }
}

export default articles
