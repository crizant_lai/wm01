import React from 'react'
import { submitForm, validateEmail, validatePassword } from './functions'

class RegistrationForm extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      doneRegistration: false,
      formData: {
        email: '',
        password: ''
      },
      formError: {}
    }
    this.inputChange = this.inputChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }
  inputChange(e){
    const { name, value } = e.target
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        [name]: value
      }
    })
  }
  onSubmit(e){
    e.preventDefault()
    const { formData } = this.state
    const formError = {}

    if (!validateEmail(formData.email)){
      formError.email = '請輸入正確電郵地址'
    }
    if (!validatePassword(formData.password)){
      formError.password = '請輸入6-12位長的英數組合'
    }

    this.setState({
      formError
    })

    if (Object.keys(formError).length===0){
      //no error
      submitForm(formData).then(()=>{
        this.setState({
          doneRegistration: true
        })
      })
    }
  }
  render(){
    const { doneRegistration, formData, formError } = this.state
    if (!doneRegistration){
      return (
        <form noValidate onSubmit={this.onSubmit}>
          <div class="form-group">
            <label>電郵地址</label>
            <input type="email" name="email" value={formData.email}
              placeholder="電郵地址" onChange={this.inputChange}/>
            {
              (formError.email!==undefined) &&
              <p class="error">
                { formError.email }
              </p>
            }
          </div>
          <div class="form-group">
            <label>密碼</label>
            <input type="password" name="password" value={formData.password}
              placeholder="密碼" onChange={this.inputChange}/>
            {
              (formError.password!==undefined) &&
              <p class="error">
                { formError.password }
              </p>
            }
          </div>
          <button type="submit">
            提交
          </button>
        </form>
      )
    } else {
      return (
        <h3>
          你已成功登記, 謝謝!
        </h3>
      )
    }
  }
}

export default RegistrationForm
