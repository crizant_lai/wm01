import { createStore, applyMiddleware } from 'redux'
import Reducers from './reducers/index'
import Middlewares from './middlewares/index'

const store = createStore(
  Reducers,
  applyMiddleware(...Middlewares)
)

export default store
