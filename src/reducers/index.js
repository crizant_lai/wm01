import { combineReducers } from 'redux'
import layout from './layout'
import lazyLoadArticles from './articles'

const Reducers = combineReducers({
  layout,
  lazyLoadArticles
})

export default Reducers
