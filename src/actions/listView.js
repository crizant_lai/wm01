const getArticles = () => {
  return {
    type: 'GET_ARTICLES'
  }
}

const getArticlesSuccess = (articles) => {
  return {
    type: 'GET_ARTICLES_SUCCESS',
    articles
  }
}

export {
  getArticles,
  getArticlesSuccess
}
