import listViewMiddleware from './listViewMiddleware'

const Middlewares = [
  listViewMiddleware
]

export default Middlewares
