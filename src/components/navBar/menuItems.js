const menuItems = [
  {
    name: "主頁",
    url: "https://www.wm01.com/"
  },
  {
    name: "新聞",
    url: "https://www.wm01.com/channel/%E6%96%B0%E8%81%9E"
  },
  {
    name: "特首選舉2017",
    url: "http://2017hkceelection.wm01.com/"
  },
  {
    name: "01觀點",
    url: "https://www.wm01.com/section/01%E8%A7%80%E9%BB%9E"
  },
  {
    name: "01博評",
    url: "https://www.wm01.com/blogger/01%E5%8D%9A%E8%A9%95"
  },
  {
    name: "社區",
    url: "https://www.wm01.com/channel/%E7%A4%BE%E5%8D%80"
  },
  {
    name: "娛樂",
    url: "https://www.wm01.com/channel/%E5%A8%9B%E6%A8%82"
  },
  {
    name: "國際",
    url: "https://www.wm01.com/section/%E5%9C%8B%E9%9A%9B"
  },
  {
    name: "女生",
    url: "https://www.wm01.com/channel/%E5%A5%B3%E7%94%9F"
  },
  {
    name: "好生活",
    url: "https://www.wm01.com/section/%E5%A5%BD%E7%94%9F%E6%B4%BB"
  },
  {
    name: "體育",
    url: "https://www.wm01.com/section/%E9%AB%94%E8%82%B2"
  },
  {
    name: "熱話",
    url: "https://www.wm01.com/channel/%E7%86%B1%E8%A9%B1"
  },
  {
    name: "科技玩物",
    url: "https://www.wm01.com/channel/%E7%A7%91%E6%8A%80%E7%8E%A9%E7%89%A9"
  },
  {
    name: "01哲學",
    url: "http://philosophy.wm01.com/"
  },
  {
    name: "01影像",
    url: "https://www.wm01.com/photostory"
  },
  {
    name: "01活動",
    url: "https://www.wm01.com/channel/01%E6%B4%BB%E5%8B%95"
  },
  {
    name: "01空間",
    url: "http://space.wm01.com/"
  },
  {
    name: "會員專區",
    url: "https://www.wm01.com/channel/%E6%9C%83%E5%93%A1%E5%B0%88%E5%8D%80"
  }
]

export default menuItems
