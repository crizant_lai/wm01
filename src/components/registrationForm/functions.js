const submitForm = function(formData){
  return new Promise(function(resolve) {
    //mock the api call
    setTimeout(resolve, 200)
  })
}

const validateEmail = function(email){
  const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return pattern.test(email)
}

const validatePassword = function(password){
  const pattern = /^[a-zA-Z0-9]{6,12}$/
  return pattern.test(password)
}

export {
  submitForm,
  validateEmail,
  validatePassword
}
